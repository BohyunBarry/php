-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 15, 2015 at 09:50 PM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `security`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'security', 'ytiruces');

-- --------------------------------------------------------

--
-- Table structure for table `staffs`
--

CREATE TABLE `staffs` (
  `staffID` int(20) NOT NULL,
  `staffName` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staffs`
--

INSERT INTO `staffs` (`staffID`, `staffName`) VALUES
(1, 'Daniel Patterson'),
(2, 'Ryan Cox'),
(3, 'Aaron Hill'),
(4, 'Michelle Robinson'),
(5, 'Joan Diaz'),
(6, 'Robert Brown'),
(7, 'Ruby Williams'),
(8, 'Shirley Bell'),
(9, 'Sandar Anderson'),
(10, 'Johnny Lee'),
(11, 'Brenda Young'),
(12, 'Marilyn Howard'),
(13, 'Jennifer Sanders'),
(14, 'Kathryn Thomas'),
(15, 'Louise Hall'),
(16, 'Linda Coleman'),
(17, 'Betty Ramirez'),
(101, 'Joyce Bryant'),
(102, 'Judith Butler'),
(103, 'Susan Perry'),
(104, 'Raymond Lewis'),
(105, 'Wayne Cook'),
(106, 'Daniel Patterson');

-- --------------------------------------------------------

--
-- Table structure for table `workitems`
--

CREATE TABLE `workitems` (
  `workID` int(20) NOT NULL,
  `staffID` int(20) DEFAULT NULL,
  `workDescription` varchar(255) DEFAULT NULL,
  `workPrice` decimal(11,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workitems`
--

INSERT INTO `workitems` (`workID`, `staffID`, `workDescription`, `workPrice`) VALUES
(1, 101, 'Manger of Hk', '300'),
(10, 102, 'Manger of FS', '280'),
(11, 103, 'Manger of Log', '320'),
(20, 11, 'Logistics Department', '130'),
(101, 105, 'Manger of MK', '380'),
(110, 1, 'Receptionist', '100'),
(111, 2, 'Guard', '80'),
(112, 3, 'Guard', '80'),
(113, 4, 'Housekeeping Department', '120'),
(114, 5, 'Housekeeping Department', '120'),
(115, 6, 'Housekeeping Department', '120'),
(116, 7, 'Housekeeping Department', '120'),
(117, 8, 'Food Service', '85'),
(118, 9, 'Food Service', '85'),
(119, 10, 'Food Service', '85'),
(121, 12, 'Logistics Department', '130'),
(122, 13, 'Ministry of Personnel', '135'),
(123, 14, 'Marketing Department', '135'),
(124, 15, 'Marketing Department', '135'),
(125, 16, 'Public Relations Department', '180'),
(126, 17, 'Public Relations Department', '180'),
(1001, 17, 'Chairman', '30000'),
(1101, 104, 'Manger of MP', '400'),
(1111, 16, 'Manger of PR', '500');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `staffs`
--
ALTER TABLE `staffs`
  ADD PRIMARY KEY (`staffID`);

--
-- Indexes for table `workitems`
--
ALTER TABLE `workitems`
  ADD PRIMARY KEY (`workID`),
  ADD KEY `staffID` (`staffID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
